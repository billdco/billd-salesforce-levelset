#!/usr/bin/env python3
from datetime import datetime
from urllib.parse import quote_plus

import json
import requests
import time

import boto3
import os


class SalesForceAPI(object):
    def __init__(self):
        self.access_token = None

        self.base_url = None
        self.default_headers = None

        self.authenticated_at = None
        self.token_expiry = 300 # estimated auth token expiry time (in seconds).


    def authenticate(self):
        # https://developer.salesforce.com/docs/atlas.en-us.api_rest.meta/api_rest/quickstart_oauth.htm
        if self.authenticated_at and self.token_expiry > (time.time() - self.authenticated_at):
            return

        # self.set_sf_credentials()
        self.security_token = "K1KmqjDl5dwsnNyJaeTAjQmU"
        self.client_id = "3MVG9d3kx8wbPieGSWxonWunuvBOnXWAbIetvVq2y4XGPQGBUiyXA4J92hiK8l0HE5P1UdpuS0qmpGnZadclH"
        self.client_secret = "E04278315AAF1E9508A77143B514D0BBC83C22C7B98D501C058EF671310CE2F1"
        self.user_name = "dev@billd.com.partial"
        self.password = "LakeAustin345@"
        self.auth_url = "https://test.salesforce.com/services/oauth2/token"

        data = {
            "grant_type": "password",
            "client_id": self.client_id,
            "client_secret": self.client_secret,
            "username": self.user_name,
            "password": self.password + self.security_token
        }
        resp = requests.post(self.auth_url, data=data)
        resp_json = resp.json()
        print("Authenticate", resp_json)
        self.access_token = resp_json.get('access_token')
        self.base_url = resp_json.get('instance_url')
        self.default_headers = {
            "Authorization": "Bearer {session_id}".format(session_id=self.access_token),
            "Content-Type": "application/json"
        }

        self.authenticated_at = time.time()


    def set_sf_credentials(self):
        aws_secret_id = os.environ["AWS_SECRET_ID"]
        aws_access_key_id = os.environ["AWS_ACCESS_KEY_ID"]
        aws_secret_access_key = os.environ["AWS_SECRET_ACCESS_KEY"]

        aws_session = boto3.session.Session()
        aws_secret_manager = aws_session.client(
            service_name="secretsmanager",
            aws_access_key_id=aws_access_key_id,
            aws_secret_access_key=aws_secret_access_key,
            region_name="us-west-2"
        )

        response = aws_secret_manager.get_secret_value(SecretId=aws_secret_id)
        secret = json.loads(response["SecretString"])

        self.user_name = secret["username"]
        self.password = secret["password"]
        self.security_token = secret["security_token"]

        self.client_id = secret["client_id"]
        self.client_secret = secret["client_secret"]
        self.auth_url = secret["auth_url"]


    def get_project_by_id(self, project_id):
        self.authenticate()

        url = "{}/services/data/v39.0/sobjects/Project__c/{}".format(self.base_url, project_id)

        resp = requests.get(url, headers=self.default_headers)
        return resp.json()


    def get_account_by_id(self, account_id):
        self.authenticate()
        url = "{}/services/data/v39.0/sobjects/Account/{}".format(self.base_url, account_id)

        resp = requests.get(url, headers=self.default_headers)
        return resp.json()


    def get_material_purchase_by_id(self, material_purchase_id):
        self.authenticate()
        url = "{}/services/data/v39.0/sobjects/Opportunity/{}".format(self.base_url, material_purchase_id)

        resp = requests.get(url, headers=self.default_headers)
        return resp.json()


    def get_material_purchase_date_from_loan(self, material_purchase_id):
        self.authenticate()

        soql = "SELECT Id, Name, Date_Supplier_Initial_Disbursement__c FROM Loan__c WHERE Material_Order__c = '{}'".format(material_purchase_id)
        url = "{}/services/data/v39.0/query/?q={}".format(self.base_url, quote_plus(soql))
        resp = requests.get(url, headers=self.default_headers)
        result = resp.json()["records"]

        return result[0].get("Date_Supplier_Initial_Disbursement__c") if len(result) else None


    def update_levelset_project(self, project_id, levelset_project_id, levelest_project_url):
        self.authenticate()

        url = "{}/services/data/v39.0/sobjects/Project__c/{}".format(self.base_url, project_id)
        data = {
            "Levelset_Project_ID__c": levelset_project_id,
            "Levelset_Project__c": levelest_project_url
        }

        resp = requests.patch(url, json=data, headers=self.default_headers)
        return {"success": resp.ok, "result": resp.text }


    def update_levelset_invoice_id(self, material_purchase_id, levelest_invoice_id):
        self.authenticate()

        url = "{}/services/data/v39.0/sobjects/Opportunity/{}".format(self.base_url, material_purchase_id)
        data = {
            "Levelset_Invoice_ID__c": levelest_invoice_id,
        }

        resp = requests.patch(url, json=data, headers=self.default_headers)
        return {"success": resp.ok, "result": resp.text }


    def create_stakeholder_account(self, account_name, email, address_street, address_city, address_state, address_zip):
        self.authenticate()

        url = "{}/services/data/v39.0/sobjects/Account".format(self.base_url)
        data = {
            "Name": account_name,
            "Business_Email__c": email,
            "BillingStreet": address_street,
            "BillingCity": address_city,
            "BillingState": address_state,
            "BillingPostalCode": address_zip,
            "RecordType": {
                "Name": "Project Stakeholder"
            }
        }
        resp = requests.post(url, json=data, headers=self.default_headers)
        return {"success": resp.ok, "result": resp.text }


    def update_project_stakeholder(self, project_id, account_stake_holder_id):
        self.authenticate()

        data = {
            "Surety_Bonding_Co__c": account_stake_holder_id
        }
        url = "{}/services/data/v39.0/sobjects/Project__c/{}".format(self.base_url, project_id)
        resp = requests.patch(url, json=data, headers=self.default_headers)

        return {"success": resp.ok, "result": resp.text }



    def update_lien_dates(self, project_id, lien_dates):
        self.authenticate()

        field_keys = {
            "notice_of_intent_deadline": "Notice_Of_Intent_Required_Date__c",
            "prelien_deadline": "Prelien_Required_Date__c",
            "lien_deadline": "Date_Required_Lien__c",
            "bond_deadline": "Date_Required_Bond__c",
            "notice_of_intent_sent_date": "Date_Notice_Of_Intent_Filed__c",
            "prelien_sent_date": "Date_Prelien_Filed__c",
            "lien_filed_date": "Date_Filed_Lien__c",
            "lien_bond_date": "Date_Filed_Bond__c",
        }

        data = {}
        for ls_key, sf_key in field_keys.items():
            if lien_dates[ls_key]:
                data[sf_key] = lien_dates[ls_key]

        if len(data):
            url = "{}/services/data/v39.0/sobjects/Project__c/{}".format(self.base_url, project_id)
            resp = requests.patch(url, json=data, headers=self.default_headers)
            return {"success": resp.ok, "result": resp.text }

        return {"success": True, "result": "Empty lien dates. Nothing to update."}


def json_print(obj):
    print(json.dumps(obj, indent=4))

if __name__ == "__main__":
    ctr = SalesForceAPI()
    # json_print(ctr.get_account_by_id("001f400001FJcvyAAD"))
    resp = ctr.get_material_purchase_by_id("0065G00000WkDIeQAN")
    json_print(resp)
    # print(ctr.get_material_purchase_date_from_loan("0065G00000WkDIeQAN"))
    # json_print(ctr.get_account_by_id("001f400000yyq7eAAA"))
    # json_print(ctr.get_business_owners("001f400001Dwh9uAAB"))
