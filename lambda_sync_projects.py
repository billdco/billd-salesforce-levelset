#!/usr/bin/env python3

from urllib import request

def lambda_handler(event, context):
    url = "http://salesforce-levelset-prod.eba-nmvum9a7.us-west-2.elasticbeanstalk.com/sync_projects?api_key=billd-salesforce-levelset-api-key-1"
    resp = request.urlopen(url)
    return {
        'statusCode': 200,
        'body': resp.read().decode("UTF-8")
    }


if __name__ == '__main__':
    print(lambda_handler(None, None))
