#!/usr/bin/env python3
from box import BoxAPI
from dynamo_db import DynamoDB
from levelset import LevelSetAPI
from salesforce import SalesForceAPI

from datetime import datetime
from datetime import timedelta

from io import BytesIO

import requests
import json
import re
import traceback

class Coordinator(object):

    def __init__(self):
        self.db = DynamoDB()
        self.levelset = LevelSetAPI()
        self.salesforce = SalesForceAPI()
        self.box = BoxAPI()


    def setup_new_levelset_project(self, sf_proj_id):
        sf_proj = self.salesforce.get_project_by_id(sf_proj_id)

        print("Salesforce Project ID: {}".format(sf_proj_id))
        print("Salesforce Project Name: {}".format(sf_proj.get("Name")))
        print("Salesforce Reference ID: {}".format(sf_proj.get("Reference_Id__c")))

        mandatory_fields = ("Name", "Street__c", "City__c", "State__c", "Zip__c")
        missing_fields = [field for field in mandatory_fields if not sf_proj.get(field)]
        if len(missing_fields):
            return {
                "success": False,
                "error": "Salesforce Project '{}' must supply the ff. field(s): {}.".format(sf_proj_id, ", ".join(missing_fields))
            }

        project = self.search_levelset_project(sf_proj)
        if project is not None:
            ls_proj_id, ls_proj_name = project["project_id"], project["project_nickname"]
            resp = { "success": True, "message": "Project '{}' already exists.".format(sf_proj.get("Name")) }
        else:
            resp = self.create_levelset_project(sf_proj)
            if not resp["success"]: return resp

            ls_proj_id = resp["project"]["project_id"]
            ls_proj_name = resp["project"]["project_nickname"]

        self.create_levelset_contacts(ls_proj_id, sf_proj)

        self.salesforce.update_levelset_project(sf_proj_id, ls_proj_id, self.levelset.get_project_url(ls_proj_id))
        self.add_project_for_periodic_sync(ls_proj_id, sf_proj_id, ls_proj_name)

        return resp


    def create_levelset_invoice(self, sf_material_purchase_id):
        try:
            sf_material_purchase = self.salesforce.get_material_purchase_by_id(sf_material_purchase_id)
            material_purchase_exists = isinstance(sf_material_purchase, dict) and sf_material_purchase.get("Id") is not None
            if not material_purchase_exists:
                return {
                    "success": False,
                    "error": "Material purchase '{}' not found.".format(sf_material_purchase_id)
                }

            sf_project = self.salesforce.get_project_by_id(sf_material_purchase.get("Project__c"))
            ls_project = self.search_levelset_project(sf_project)
            ls_invoice = self.search_levelset_invoice(sf_material_purchase, ls_project)

            ls_project_id = ls_project["project_id"]
            ls_invoice_external_id = sf_material_purchase.get("Name")
            ls_invoice_date = sf_material_purchase.get("Date_ACH_Sent__c")
            ls_invoice_amount = sf_material_purchase.get("LoanAmount__c")
            ls_invoice_balance = sf_material_purchase.get("Loan_Balance__c")
            ls_invoice_status_paid = sf_material_purchase.get("StageName") == "Paid"

            if ls_invoice_date is None:
                # Get date from Loan
                ls_invoice_date = self.salesforce.get_material_purchase_date_from_loan(sf_material_purchase_id)

            if ls_invoice is not None:
                assert ls_invoice["project_id"] == ls_project["project_id"]

                resp = self.levelset.update_invoice(ls_invoice["invoice_id"], ls_invoice_external_id, ls_invoice_date, ls_invoice_amount, ls_invoice_balance, ls_invoice_status_paid)
                result = dict(operation="update", old_invoice=ls_invoice, new_invoice=resp)
                ls_invoice_id = ls_invoice["invoice_id"]
            else:
                resp = self.levelset.create_invoice(ls_project_id, ls_invoice_external_id, ls_invoice_date, ls_invoice_amount, ls_invoice_balance, ls_invoice_status_paid)
                result = dict(operation="create", new_invoice=resp)
                ls_invoice_id = resp["invoice_id"]

            if ls_invoice_date:
                # Update Material/Invoice delivered dates on project level.
                parse_date = lambda str_date: datetime.strptime(str_date, "%Y-%m-%d")

                date_delivered = parse_date(ls_invoice_date) + timedelta(days=1)

                first_delivered_date = self.levelset.get_invoice_first_delivered_date(ls_project_id)
                last_delivered_date = self.levelset.get_invoice_last_delivered_date(ls_project_id)

                if not first_delivered_date or parse_date(first_delivered_date) > date_delivered:
                    self.levelset.set_invoice_first_delivered_date(ls_project_id, date_delivered.strftime("%Y-%m-%d"))

                if not last_delivered_date or parse_date(last_delivered_date) < date_delivered:
                    self.levelset.set_invoice_last_delivered_date(ls_project_id, date_delivered.strftime("%Y-%m-%d"))
                    self.levelset.set_project_material_description(ls_project_id, sf_material_purchase.get("Material_Description__c"))

            if sf_material_purchase.get("Levelset_Invoice_ID__c") != ls_invoice_id:
                resp = self.salesforce.update_levelset_invoice_id(sf_material_purchase_id, ls_invoice_id)
                print("Updated LS invoice ID in SF:", resp)

            return result

        except Exception as e:
            print("Failed to create/update invoice. Material purchase ID: {}".format(sf_material_purchase_id))
            raise e


    def update_levelset_invoice(self, sf_material_purchase_id):
        try:
            sf_material_purchase = self.salesforce.get_material_purchase_by_id(sf_material_purchase_id)
            material_purchase_exists = isinstance(sf_material_purchase, dict) and sf_material_purchase.get("Id") is not None
            if not material_purchase_exists:
                return {
                    "success": False,
                    "error": "Material purchase '{}' not found.".format(sf_material_purchase_id)
                }

            sf_project = self.salesforce.get_project_by_id(sf_material_purchase.get("Project__c"))
            ls_project = self.search_levelset_project(sf_project)
            ls_invoice = self.search_levelset_invoice(sf_material_purchase, ls_project)

            ls_project_id = ls_project["project_id"]
            ls_invoice_external_id = sf_material_purchase.get("Name")
            ls_invoice_date = sf_material_purchase.get("Supplier_Disbursement_Date__c")
            ls_invoice_amount = sf_material_purchase.get("LoanAmount__c")
            ls_invoice_balance = sf_material_purchase.get("Loan_Balance__c")
            ls_invoice_status_paid = sf_material_purchase.get("StageName") == "Paid"

            if ls_invoice_date is None:
                # Get date from Loan
                ls_invoice_date = self.salesforce.get_material_purchase_date_from_loan(sf_material_purchase_id)

            if ls_invoice is not None:
                assert ls_invoice["project_id"] == ls_project["project_id"]

                resp = self.levelset.update_invoice(ls_invoice["invoice_id"], ls_invoice_external_id, ls_invoice_date, ls_invoice_amount, ls_invoice_balance, ls_invoice_status_paid)
                result = dict(operation="update", old_invoice=ls_invoice, new_invoice=resp)
                ls_invoice_id = ls_invoice["invoice_id"]
            else:
                resp = self.levelset.create_invoice(ls_project_id, ls_invoice_external_id, ls_invoice_date, ls_invoice_amount, ls_invoice_balance, ls_invoice_status_paid)
                result = dict(operation="create", new_invoice=resp)
                ls_invoice_id = resp["invoice_id"]

            if ls_invoice_date:
                # Update Material/Invoice delivered dates on project level.
                parse_date = lambda str_date: datetime.strptime(str_date, "%Y-%m-%d")

                date_delivered = parse_date(ls_invoice_date) + timedelta(days=1)

                first_delivered_date = self.levelset.get_invoice_first_delivered_date(ls_project_id)
                last_delivered_date = self.levelset.get_invoice_last_delivered_date(ls_project_id)

                if not first_delivered_date or parse_date(first_delivered_date) > date_delivered:
                    self.levelset.set_invoice_first_delivered_date(ls_project_id, date_delivered.strftime("%Y-%m-%d"))

                if not last_delivered_date or parse_date(last_delivered_date) < date_delivered:
                    self.levelset.set_invoice_last_delivered_date(ls_project_id, date_delivered.strftime("%Y-%m-%d"))
                    self.levelset.set_project_material_description(ls_project_id, sf_material_purchase.get("Material_Description__c"))

            if sf_material_purchase.get("Levelset_Invoice_ID__c") != ls_invoice_id:
                resp = self.salesforce.update_levelset_invoice_id(sf_material_purchase_id, ls_invoice_id)
                print("Updated LS invoice ID in SF:", resp)

            return result

        except Exception as e:
            print("Failed to create/update invoice. Material purchase ID: {}".format(sf_material_purchase_id))
            raise e


    def create_levelset_project(self, sf_proj):
        resp = self.levelset.filter_states_by_abbreviation(sf_proj.get("State__c"))
        states = resp.get("_embedded", {}).get("States", [])
        if not len(states):
            resp = self.levelset.filter_states_by_name(sf_proj.get("State__c"))
            states = resp.get("_embedded", {}).get("States", [])
            if not len(states):
                return {
                    "success": False,
                    "error": "Unable to get state ID. Please make sure the given State name or abbreviation is valid."
                }

        state_id = int(states[0].get("state_id")) if len(states) else 0

        reference_id = sf_proj.get("Reference_Id__c")
        external_id = re.match("<a .+?>(.+?)</a>", reference_id).group(1) if reference_id else None

        project_type_id = {
            "Residential": 1,
            "Owner Occupied Residential": 3,
            "Commercial/Industrial": 4,
            "State/County": 5,
            "Federal": 6
        }.get(sf_proj.get("Project_Type__c"), 4)

        payload = {
            "project_type_id": project_type_id,
            "project_address": sf_proj.get("Street__c"),
            "project_city": sf_proj.get("City__c"),
            "state_id": state_id,
            "project_zip": sf_proj.get("Zip__c"),
            "project_nickname": sf_proj.get("Name"),
            # "project_status": true,
            "contractor_role_id": 5,
            # "hired_role_id": 2 if sf_proj.get("Contractor_Account__c") == sf_proj.get("General_Contractor_Account__c") else 3,
            "hired_role_id": 3,
            "project_labor_description": "Labor and/or Materials",
            # "project_external_identifier": external_id,
            "outstanding_debt": 0.000,
            "location_id": 72929,
            "is_researched": False,
            "line_of_business_id": 68503,
        }

        result = self.levelset.create_project(payload)
        return {
            "success": result["success"],
            "project": result["result"]
        }


    def get_projects_for_periodic_sync(self):
        return list(self.db.get_all_projects())


    def add_project_for_periodic_sync(self, ls_proj_id, sf_proj_id, ls_proj_name):
        return self.db.add_project(ls_proj_id, sf_proj_id, ls_proj_name)



    def remove_project_for_periodic_sync(self, sf_proj_id, ls_proj_id=None):
        sf_proj = self.salesforce.get_project_by_id(sf_proj_id)
        project = self.search_levelset_project(sf_proj)
        if project is None:
            return {
                "success": False,
                "error": "Project '{}' not found in Levelset.".format(sf_proj.get("Name"))
            }

        ls_proj_id = project["project_id"]
        result = self.db.remove_project(ls_proj_id)
        return {
            "success": True,
            "message": "Levelset project {} removed from periodic sync.".format(ls_proj_id),
            "result": result if not isinstance(result, dict) else json.dumps(result)
        }


    def sync_project(self, ls_proj_id, sf_proj_id):
        levelset_project = self.levelset.get_project_by_id(ls_proj_id)
        levelset_project_id = levelset_project.get("project_id")
        if levelset_project_id is None:
            return {
                "success": False,
                "message": "Levelset project '{}' not found.".format(ls_proj_id)
            }

        if sf_proj_id is not None:
            # Sync Levelset project surety contact to Salesforce as project sakeholder.
            try:
                self.sync_levelset_surety_contact_to_salesforce(levelset_project_id, sf_proj_id)
            except:
                print("Sync Levelset Surety contact to Salesforce failed: {} - {}".format(levelset_project_id, sf_proj_id))
                traceback.print_exc()

            # Sync lien dates:
            lien_dates = self.levelset.get_all_lien_dates(levelset_project_id)
            print("LIEN DATES SYNC: {}, {}".format(sf_proj_id, ls_proj_id), lien_dates, self.salesforce.update_lien_dates(sf_proj_id, lien_dates))

        # Sync documents:
        resp = self.levelset.get_project_documents(ls_proj_id)
        documents = resp.get("_embedded", {}).get("Documents", [])
        document_urls = list(filter(None, [doc.get("document_file_url") for doc in documents]))
        synced_document_urls = self.db.get_project_synced_docs(ls_proj_id)

        if set(document_urls) == set(synced_document_urls):
             return {
                "success": True,
                "project_id": ls_proj_id,
                "num_document": "No new files to upload."
            }

        new_document_urls = set(document_urls) - set(synced_document_urls)
        documents = []
        for url in new_document_urls:
            resp = requests.get(url, stream=True)
            filename = re.search(r'filename="(.+)"', resp.headers["Content-Disposition"]).group(1)

            documents.append({
                "filename": filename,
                "content": BytesIO(resp.content)
            })

        if len(documents):
            self.box.upload_files(levelset_project["project_nickname"], documents)

        self.db.update_synced_document(ls_proj_id, document_urls)
        return {
            "success": True,
            "project_id": ls_proj_id,
            "num_document_synced": len(documents)
        }


    def sync_levelset_surety_contact_to_salesforce(self, ls_project_id, sf_project_id):
        """
        If a Levelset project has a contact with a role "Surety / Bonding Co." (role ID: 15),
            sync it to Salesforce as account (Stakeholder account of the project)
        """
        SURETY_BONDING_CO_ROLE_ID = 15

        if not (ls_project_id and sf_project_id):
            return

        sf_project = self.salesforce.get_project_by_id(sf_project_id)
        if not sf_project:
            return

        if sf_project.get("Surety_Bonding_Co__c"): # Ignore there is surety account already
            return

        resp = self.levelset.get_project_contacts(ls_project_id)
        project_contacts = resp.get("_embedded", {}).get("ProjectContacts", [])
        surety_contacts = list(filter(
            lambda contact: int(contact.get("role_id")) == SURETY_BONDING_CO_ROLE_ID,
            project_contacts
        ))
        if not len(surety_contacts):
            return

        surety_contact = surety_contacts[0]
        contact_details = self.levelset.get_contact_details(surety_contact["contact_id"])

        resp = self.salesforce.create_stakeholder_account(
            contact_details.get("contact_company"),
            contact_details.get("contact_email"),
            contact_details.get("contact_address"),
            contact_details.get("contact_city"),
            contact_details.get("contact_state"),
            contact_details.get("contact_zip")
        )
        if resp.get("success"):
            stakeholder_account_id = json.loads(resp.get("result"))["id"]

        resp = self.salesforce.update_project_stakeholder(sf_project_id, stakeholder_account_id)


    def sync_all_projects(self):
        projects = self.db.get_all_projects()
        num_projects = len(projects)
        for i, project in enumerate(projects):
            print("SYNCING {}/{}:".format(i + 1, num_projects), project["project_id"], project.get("sf_project_id"))
            resp = self.sync_project(project["project_id"], project.get("sf_project_id"))
            print(resp)

    def create_levelset_contacts(self, ls_project_id, sf_proj):
        gc_role_id, subcontractor_role_id = 2, 3

        # If the contractor account is the same as GC, then tag the GC contact as the customer in Levelset.
        if sf_proj["Contractor_Account__c"] == sf_proj["General_Contractor_Account__c"]:
            self.levelset.update_project_hired_role(ls_project_id, gc_role_id)
            self.create_levelset_contact(sf_proj["General_Contractor_Account__c"], ls_project_id, gc_role_id, is_customer=True)
        else:
            self.levelset.update_project_hired_role(ls_project_id, subcontractor_role_id)
            self.create_levelset_contact(sf_proj["Contractor_Account__c"], ls_project_id, subcontractor_role_id, is_customer=True)
            self.create_levelset_contact(sf_proj["General_Contractor_Account__c"], ls_project_id, gc_role_id)

        self.create_levelset_contact(sf_proj["Property_Owner_Account__c"], ls_project_id, 1)
        self.create_levelset_contact(sf_proj["Project_Owner__c"], ls_project_id, 1)
        self.create_levelset_contact(sf_proj["Engineer_Account__c"], ls_project_id, 8)
        self.create_levelset_contact(sf_proj["Architect_Account__c"], ls_project_id, 8)


    def create_levelset_contact(self, sf_account_id, ls_project_id, ls_role_id, is_customer=False):
        if not sf_account_id:
            return

        account = self.salesforce.get_account_by_id(sf_account_id)
        account_address = account["BillingAddress"] if account["BillingAddress"] is not None else {}
        self.levelset.create_project_contact(
            ls_project_id,
            ls_role_id,
            is_customer,
            account["Name"],
            account_address.get("street"),
            account_address.get("city"),
            account_address.get("state"),
            account_address.get("postalCode"),
            account["Business_Email__c"],
            account["Phone"]
        )


    def search_levelset_project(self, sf_project):
        ls_project_id = sf_project.get("Levelset_Project_ID__c")
        ls_project_name = sf_project.get("Name")
        ls_proj_url = sf_project.get("Levelset_Project__c")

        if ls_project_id is not None and ls_project_id != "":
            project = self.levelset.get_project_info(ls_project_id)

            return project if project.get("project_id") is not None else None

        projects = self.levelset.filter_projects_by_name(ls_project_name)
        if len(projects):
            if ls_proj_url is not None and isinstance(ls_proj_url, str) and ls_proj_url.strip() != "":
                ls_project_id = self.levelset.extract_project_id_from_url(ls_proj_url)
                if ls_project_id is not None:
                    filtered_projects = list(filter(lambda project: project["project_id"] == ls_project_id, projects))
                    return filtered_projects[0] if len(filtered_projects) else projects[0]

            return projects[0]


    def search_levelset_invoice(self, sf_material_purchase, ls_project):
        invoice_id = sf_material_purchase.get("Levelset_Invoice_ID__c")
        if invoice_id is not None and isinstance(invoice_id, str) and invoice_id.strip() != "":
            invoice = self.levelset.get_invoice_by_id(invoice_id)
            if invoice.get("invoice_id") is not None:
                assert invoice["project_id"] == ls_project["project_id"]
                print("Invoice ID is used for search!")
                return invoice

        result = self.levelset.filter_invoice_by_external_id(ls_project["project_id"], sf_material_purchase["Name"])
        invoices = result.get("_embedded", {}).get("Invoices", [])
        if len(invoices):
            return invoices[0]

        result = self.levelset.filter_invoice_by_external_id(ls_project["project_id"], sf_material_purchase["Id"])
        invoices = result.get("_embedded", {}).get("Invoices", [])
        if len(invoices):
            return invoices[0]


def json_print(obj):
    print(json.dumps(obj, indent=4))


if __name__ == "__main__":
    ctr = Coordinator()
    json_print(ctr.salesforce.get_account_by_id("001f400001DUPrYAAX"))
    # json_print(ctr.sync_project("5004116"))
    # json_print(ctr.create_or_update_levelset_invoice("006f400000TrlLWAAZ"))
    # json_print(ctr.create_levelset_project("a00f400000VDIw5AAH"))

