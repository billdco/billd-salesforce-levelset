#!/usr/bin/env python3
import requests
import json

import re
import time

PROJECT_TYPES_MAP = {
    "Commercial/Industrial": "Commercial",
    "Residential": "Residential",
    "Government": "Other",
    "Agriculture": "Other",
}


class LevelSetAPI(object):

    def __init__(self):
        # self._base_url = "https://api-sandbox.api.zlien.com"
        self._base_url = "https://api.zlien.com"

        self.client_id = "0274562d2f726070988d91201dce8404978cc206494562df805a9d2f19759c35"
        self.client_secret = "7dcaf07e39e218bb27e28b0110c7729cdd319dff1ee1a6547a6dfd33567bd892"

        self.authenticated_at = None
        self.token_expiry = None


    @property
    def base_url(self):
        # print(self._base_url)
        return self._base_url


    def authenticate(self):
        data = {
            "grant_type": "client_credentials",
            "client_id": self.client_id,
            "client_secret": self.client_secret,
        }
        resp = requests.post("{}/oauth/token".format(self.base_url), data=data)
        return resp.json()


    def authenticate_if_needed(self):
        if self.authenticated_at and self.token_expiry and self.token_expiry > (time.time() - self.authenticated_at):
            return

        resp = self.authenticate()
        print("Authenticating:", resp)

        self.access_token = resp["access_token"]
        self.token_expiry = resp["expires_in"] / 60
        self.default_headers = {
            "Authorization": "Bearer {session_id}".format(session_id=self.access_token),
            "Content-Type": "application/json"
        }
        self.authenticated_at = time.time()


    def filter_projects_by_name(self, project_name, exact_match=True, case_sensitive=True):
        self.authenticate_if_needed()

        url = "{}/rest/projects/?filter[project_nickname]={}".format(self.base_url, project_name)
        resp = requests.get(url, headers=self.default_headers)
        projects = resp.json().get("_embedded", {}).get("Projects", [])

        if exact_match:
            if case_sensitive:
                matcher_fn = lambda project: project.get("project_nickname") == project_name
            else:
                matcher_fn = lambda project: project.get("project_nickname").lower() == project_name.lower()

            projects = list(filter(matcher_fn, projects))

        return projects


    def filter_projects_by_external_id(self, external_id, exact_match=True, case_sensitive=True):
        self.authenticate_if_needed()

        url = "{}/rest/projects/?filter[project_external_identifier]={}".format(self.base_url, external_id)
        resp = requests.get(url, headers=self.default_headers)
        projects = resp.json().get("_embedded", {}).get("Projects", [])

        if exact_match:
            if case_sensitive:
                matcher_fn = lambda project: project.get("project_external_identifier") == external_id
            else:
                matcher_fn = lambda project: project.get("project_external_identifier").lower() == project_external_identifier.lower()

            projects = list(filter(matcher_fn, projects))

        return projects


    def filter_states_by_abbreviation(self, state_abbreviation):
        self.authenticate_if_needed()

        url = "{}/rest/states?filter[state_abbreviation]={}".format(self.base_url, state_abbreviation)
        resp = requests.get(url, headers=self.default_headers)

        return resp.json()


    def filter_states_by_name(self, state_name):
        self.authenticate_if_needed()

        url = "{}/rest/states?filter[state_name]={}".format(self.base_url, state_name)
        resp = requests.get(url, headers=self.default_headers)

        return resp.json()


    def get_project_by_id(self, project_id):
        self.authenticate_if_needed()

        resp = requests.get("{}/rest/projects/{}".format(self.base_url, project_id), headers=self.default_headers)
        return resp.json()


    def get_project_documents(self, project_id):
        self.authenticate_if_needed()

        resp = requests.get("{}/rest/documents?filter[project_id]={}&limit=100".format(self.base_url, project_id), headers=self.default_headers)
        return resp.json()


    def get_project_info(self, project_id):
        self.authenticate_if_needed()

        resp = requests.get("{}/rest/projects/{}".format(self.base_url, project_id), headers=self.default_headers)
        return resp.json()


    def create_project(self, project_data):
        self.authenticate_if_needed()

        resp = requests.post("{}/rest/projects".format(self.base_url), json=project_data, headers=self.default_headers)
        return {
            "success": resp.ok,
            "result": resp.json()
        }


    def get_states(self):
        self.authenticate_if_needed()

        resp = requests.get("{}/rest/states".format(self.base_url), headers=self.default_headers)
        return resp.json()


    def get_project_invoices(self, project_id):
        self.authenticate_if_needed()

        resp = requests.get("{}/rest/projects/{}/invoices".format(self.base_url, project_id), headers=self.default_headers)
        return resp.json()


    def get_invoice_by_id(self, invoice_id):
        self.authenticate_if_needed()

        resp = requests.get("{}/rest/invoices/{}".format(self.base_url, invoice_id), headers=self.default_headers)
        return resp.json()


    def create_invoice(self, project_id, external_id, date, total_value, outstanding_bal, status):
        self.authenticate_if_needed()

        data = {
            "invoice_original_value": float(total_value) if total_value is not None else 0.0,
            "outstanding_invoice_value": float(outstanding_bal) if outstanding_bal is not None else 0.0,
            "invoice_date": date,
            "invoice_external_identifier": external_id,
            "invoice_status": status
        }
        resp = requests.post("{}/rest/projects/{}/invoices".format(self.base_url, project_id), json=data, headers=self.default_headers)
        if not resp.ok:
            print("Unable to create invoice. Project ID: {}".format(project_id), data)
        resp.raise_for_status()

        return resp.json()


    def update_invoice(self, invoice_id, external_id, date, total_value, outstanding_bal, status):
        self.authenticate_if_needed()

        data = {
            "invoice_original_value": float(total_value) if total_value is not None else 0.0,
            "outstanding_invoice_value": float(outstanding_bal) if outstanding_bal is not None else 0.0,
            "invoice_date": date,
            "invoice_external_identifier": external_id,
            "invoice_status": status
        }
        resp = requests.patch("{}/rest/invoices/{}".format(self.base_url, invoice_id), json=data, headers=self.default_headers)
        if not resp.ok:
            print("Unable to create invoice. Invoice ID: {}".format(invoice_id), data)

        resp.raise_for_status()

        return resp.json()


    def filter_invoice_by_external_id(self, project_id, external_id):
        self.authenticate_if_needed()

        resp = requests.get("{}/rest/projects/{}/invoices?filter[invoice_external_identifier]={}".format(
            self.base_url,
            project_id,
            external_id
        ), headers=self.default_headers)

        return resp.json()



    def get_invoice_by_id(self, invoice_id):
        self.authenticate_if_needed()

        resp = requests.get("{}/rest/invoices/{}".format(self.base_url, invoice_id), headers=self.default_headers)
        return resp.json()


    def mark_invoice_as_paid(self, invoice_id):
        self.authenticate_if_needed()

        data = { "invoice_status": True }
        resp = requests.patch("{}/rest/invoices/{}".format(self.base_url, invoice_id), json=data, headers=self.default_headers)
        resp.raise_for_status()

        return resp.json()


    def extract_project_id_from_url(self, url):
        try:
            project_id = re.search("https://app.levelset.com/project/summary/([0-9]+)", url).group(1)
        except:
            project_id = None

        return project_id


    def get_project_url(self, project_id):
        return "https://app.levelset.com/project/summary/{}".format(project_id)


    def create_or_get_contact(self, contact_company, contact_street, contact_city, contact_state, contact_zip, contact_email, contact_phone):
        self.authenticate_if_needed()

        url = "{}/rest/contacts?filter[contact_company]={}".format(self.base_url, contact_company)
        resp = requests.get(url, headers=self.default_headers)

        contacts = resp.json().get("_embedded", {}).get("Contacts", [])
        contacts = list(filter(lambda contact: contact.get("contact_company").lower() == contact_company.lower(), contacts))

        if len(contacts):
            return contacts[0]

        url = "{}/rest/contacts/".format(self.base_url)
        data = {
            "contact_company": contact_company,
            "contact_address": contact_street,
            "contact_city": contact_city,
            "contact_state": contact_state,
            "contact_zip": contact_zip,
            # "contact_email": contact_email,
            "contact_phone": contact_phone
        }
        resp = requests.post(url, json=data, headers=self.default_headers)
        resp.raise_for_status()

        return resp.json()


    def create_project_contact(self, project_id, role_id, is_customer, contact_company, contact_street, contact_city, contact_state, contact_zip, contact_email, contact_phone):
        self.authenticate_if_needed()

        contact_info = self.create_or_get_contact(contact_company, contact_street, contact_city, contact_state, contact_zip, contact_email, contact_phone)
        url = "{}/rest/projects/{}/project_contacts?filter[contact_id]={}&filter[role_id]={}".format(self.base_url, project_id, contact_info["contact_id"], role_id)
        resp = requests.get(url, headers=self.default_headers)

        project_contacts = resp.json().get("_embedded", {}).get("ProjectContacts", [])
        if len(project_contacts):
            return

        url = "{}/rest/projects/{}/project_contacts".format(self.base_url, project_id)
        data = {
            "project_id": int(project_id),
            "contact_id": int(contact_info["contact_id"]),
            "role_id": int(role_id),
            "is_customer": is_customer
        }
        resp = requests.post(url, json=data, headers=self.default_headers)
        resp.raise_for_status()

        return resp.json()


    def get_project_contacts(self, project_id):
        self.authenticate_if_needed()

        url = "{}/rest/projects/{}/project_contacts".format(self.base_url, project_id)
        resp = requests.get(url, headers=self.default_headers)
        resp.raise_for_status()

        return resp.json()


    def get_contact_details(self, contact_id):
        self.authenticate_if_needed()

        url = "{}/rest/contacts/{}".format(self.base_url, contact_id)
        resp = requests.get(url, headers=self.default_headers)
        resp.raise_for_status()

        return resp.json()


    def update_project_hired_role(self, project_id, role_id):
        self.authenticate_if_needed()

        url = "{}/rest/projects/{}".format(self.base_url, project_id)
        data = {
            "hired_role_id": role_id,
        }

        resp = requests.patch(url, json=data, headers=self.default_headers)
        resp.raise_for_status()
        return resp.json()


    def set_invoice_first_delivered_date(self, project_id, date):
        self.authenticate_if_needed()

        date_type_id = 4
        return self.create_project_date(project_id, date_type_id, date)


    def set_invoice_last_delivered_date(self, project_id, date):
        self.authenticate_if_needed()

        date_type_id = 5
        return self.create_project_date(project_id, date_type_id, date)


    def get_invoice_first_delivered_date(self, project_id):
        self.authenticate_if_needed()

        date_type_id = 4
        url = "{}/rest/projects/{}/dates".format(self.base_url, project_id)
        resp = requests.get(url, headers=self.default_headers)

        dates = list(filter(lambda d: int(d["date_type_id"]) == date_type_id, resp.json().get("_embedded", {}).get("Dates")))
        return dates[0]["date"] if len(dates) else None


    def get_invoice_last_delivered_date(self, project_id):
        self.authenticate_if_needed()

        date_type_id = 5
        url = "{}/rest/projects/{}/dates".format(self.base_url, project_id)
        resp = requests.get(url, headers=self.default_headers)

        dates = list(filter(lambda d: int(d["date_type_id"]) == date_type_id, resp.json().get("_embedded", {}).get("Dates")))
        return dates[0]["date"] if len(dates) else None


    def set_project_material_description(self, project_id, material_description):
        if material_description is not None and material_description != "":
            url = "{}/rest/projects/{}".format(self.base_url, project_id)
            data = {
                "project_labor_description": material_description,
            }
            return requests.patch(url, json=data, headers=self.default_headers)



    def create_project_date(self, project_id, date_type_id, date):
        self.authenticate_if_needed()
        data = {
            "date_type_id": date_type_id,
            "date": date,
        }
        resp = requests.post("{}/rest/projects/{}/dates".format(self.base_url, project_id), json=data, headers=self.default_headers)
        resp.raise_for_status()

        return resp.json()


    def get_project_deadlines(self, project_id):
        self.authenticate_if_needed()

        url = "{}/rest/projects/{}/deadlines?limit=100".format(self.base_url, project_id)
        resp = requests.get(url, headers = self.default_headers)

        deadlines = resp.json().get("_embedded", {}).get("Deadlines")
        for i in range(len(deadlines)):
            deadline_type = self.get_deadline_type(deadlines[i]["deadline_type_id"])
            deadlines[i]["deadline_name"] = deadline_type["deadline_type_name"].upper()

        return deadlines


    def get_project_dates(self, project_id):
        self.authenticate_if_needed()

        url = "{}/rest/projects/{}/dates?limit=100".format(self.base_url, project_id)
        resp = requests.get(url, headers=self.default_headers)

        dates = resp.json().get("_embedded", {}).get("Dates")
        for i in range(len(dates)):
            date_type = self.get_date_type(dates[i]["date_type_id"])
            dates[i]["date_name"] = date_type["date_type_name"].upper()

        return dates


    def get_deadline_type(self, deadline_type_id):
        self.authenticate_if_needed()
        url = "{}/rest/deadline_types/{}".format(self.base_url, deadline_type_id)

        resp = requests.get(url, headers = self.default_headers)
        return resp.json()


    def get_date_type(self, date_type_id):
        self.authenticate_if_needed()
        url = "{}/rest/date_types/{}".format(self.base_url, date_type_id)

        resp = requests.get(url, headers = self.default_headers)
        return resp.json()

    def get_lines_of_business(self):
        self.authenticate_if_needed()

        url = "{}/rest/contractor_roles".format(self.base_url)
        resp = requests.get(url, headers = self.default_headers)
        print(resp.text)
        return resp.json()


    # def filter_projects_by_external_id(self, external_identifier, exact_match=True, case_sensitive=True):
    #     self.authenticate_if_needed()

    #     url = "{}/rest/projects/?filter[project_external_identifier]={}".format(self.base_url, external_identifier)
    #     resp = requests.get(url, headers=self.default_headers)
    #     projects = resp.json().get("_embedded", {}).get("Projects", [])

    #     if exact_match:
    #         if case_sensitive:
    #             matcher_fn = lambda project: project.get("project_external_identifier") == external_identifier
    #         else:
    #             matcher_fn = lambda project: project.get("project_external_identifier").lower() == external_identifier.lower()

    #         projects = list(filter(matcher_fn, projects))

    #     return projects


    def get_all_lien_dates(self, project_id):
        deadline_prelien_keywords = ("PRELIMINARY NOTICE DUE",)
        deadline_notice_of_intent_keywords = ("NOTICE OF INTENT TO LIEN", "NOTICE TO OWNER OF INTENTION TO CLAIM A LIEN")
        deadline_lien_keywords = ("FILE MECHANICS LIEN WITH ULTIMATE PROTECTION", "FILE CLAIM OF LIEN", "FILING OF ClAIM OF LIEN DUE", "FILE NON-RESIDENTIAL CLAIM OF LIEN", "POTENTIAL LIEN DEADLINE UPCOMING")
        deadline_bond_keywords = ()

        date_prelien_sent_keywords = ("PRELIMINARY NOTICE", "SUBCONTRACTOR IDENTIFICATION NOTICE SENT")
        date_notice_of_intent_sent_keywords = ("NOTICE OF INTENT TO LIEN",)
        date_lien_sent_keywords = ("DATE LIEN FILED WITH COUNTY RECORDING OFFICE",)
        date_bond_keywords = ("DATE LIEN BONDED / CANCELLED",)

        project_deadlines = self.get_project_deadlines(project_id)
        project_dates = self.get_project_dates(project_id)

        filter_key = lambda deadline: any(keyword in deadline["deadline_name"] for keyword in deadline_prelien_keywords)
        prelien_deadlines = list(filter(filter_key, project_deadlines))
        prelien_deadline = prelien_deadlines[-1]["deadline_date"] if len(prelien_deadlines) else None

        filter_key = lambda deadline: any(keyword in deadline["deadline_name"] for keyword in deadline_notice_of_intent_keywords)
        notice_of_intent_deadlines = list(filter(filter_key, project_deadlines))
        notice_of_intent_deadline = notice_of_intent_deadlines[-1]["deadline_date"] if len(notice_of_intent_deadlines) else None

        filter_key = lambda deadline: any(keyword in deadline["deadline_name"] for keyword in deadline_lien_keywords)
        lien_deadlines = list(filter(filter_key, project_deadlines))
        lien_deadline = lien_deadlines[-1]["deadline_date"] if len(lien_deadlines) else None

        filter_key = lambda deadline: any(keyword in deadline["deadline_name"] for keyword in deadline_bond_keywords)
        bond_deadlines = list(filter(filter_key, project_deadlines))
        bond_deadline = bond_deadlines[-1]["deadline_date"] if len(bond_deadlines) else None

        filter_key = lambda date: any(keyword in date["date_name"] for keyword in date_prelien_sent_keywords)
        prelien_sent_dates = list(filter(filter_key, project_dates))
        prelien_sent_date = prelien_sent_dates[-1]["date"] if len(prelien_sent_dates) else None

        filter_key = lambda date: any(keyword in date["date_name"] for keyword in date_notice_of_intent_sent_keywords)
        notice_of_intent_sent_dates = list(filter(filter_key, project_dates))
        notice_of_intent_sent_date = notice_of_intent_sent_dates[-1]["date"] if len(notice_of_intent_sent_dates) else None

        filter_key = lambda date: any(keyword in date["date_name"] for keyword in date_lien_sent_keywords)
        lien_filed_dates = list(filter(filter_key, project_dates))
        lien_filed_date = lien_filed_dates[-1]["date"] if len(lien_filed_dates) else None

        filter_key = lambda date: any(keyword in date["date_name"] for keyword in date_bond_keywords)
        lien_bond_dates = list(filter(filter_key, project_dates))
        lien_bond_date = lien_bond_dates[-1]["date"] if len(lien_bond_dates) else None

        return {
            "notice_of_intent_deadline": notice_of_intent_deadline,
            "prelien_deadline": prelien_deadline,
            "lien_deadline": lien_deadline,
            "bond_deadline": bond_deadline,
            "notice_of_intent_sent_date": notice_of_intent_sent_date,
            "prelien_sent_date": prelien_sent_date,
            "lien_filed_date": lien_filed_date,
            "lien_bond_date": lien_bond_date
        }


def json_print(obj):
    print(json.dumps(obj, indent=4))

if __name__ == '__main__':
    ctr = LevelSetAPI()

    # json_print(ctr.filter_states_by_name("Texas"))
    # json_print(ctr.filter_states_by_abbreviation("TX"))

    project_id = "6669773"
    # json_print(ctr.filter_projects_by_external_id("Project-01319"))
    ls_invoice_date = '2020-10-21'

    parse_date = lambda str_date: datetime.strptime(str_date, "%Y-%m-%d")
    date_delivered = parse_date(ls_invoice_date) + timedelta(days=1)

    json_print(ctr.get_invoice_first_delivered_date(project_id))

    # json_print(ctr.get_project_info("5737730"))
    # json_print(ctr.get_project_documents("5737730"))
