#!/usr/bin/env python3
from datetime import datetime

import boto3
import json


class DynamoDB(object):

    def __init__(self):
        self.db = boto3.resource(
            "dynamodb",
            aws_access_key_id="AKIAZHDDQQT3G5NCBAW2",
            aws_secret_access_key="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",
            region_name="us-west-2"
        )
        self.table = self.db.Table("salesforce-levelset-project-sync")


    def add_project(self, project_id, sf_project_id, project_name):
        resp = self.table.get_item(Key={"project_id": project_id})
        if not resp.get("Item"):
            resp = self.table.put_item(Item={
                "project_id": project_id,
                "sf_project_id": sf_project_id,
                "project_name": project_name,
                "created_at": datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S")
            })

        return resp


    def get_project_synced_docs(self, project_id):
        resp = self.table.get_item(Key={"project_id": project_id})
        return resp.get("Item", {}).get("synced_document_urls", [])


    def update_synced_document(self, project_id, synced_document_urls):
        resp = self.table.get_item(Key={"project_id": project_id})
        if resp.get("Item"):
            resp = self.table.update_item(
                Key={"project_id": project_id},
                UpdateExpression="set synced_document_urls = :docs",
                ExpressionAttributeValues={
                    ':docs': synced_document_urls,
                },
                ReturnValues="UPDATED_NEW"
            )

        return resp



    def remove_project(self, project_id):
        resp = self.table.delete_item(Key={"project_id": project_id})
        return resp


    def get_all_projects(self):
        resp = self.table.scan()
        return resp.get("Items", [])


def json_print(obj):
    print(json.dumps(obj, indent=4))

if __name__ == '__main__':
    ctr = DynamoDB()
    json_print(list(ctr.get_all_projects()))
