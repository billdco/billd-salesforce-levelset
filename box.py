#!/usr/bin/env python3
from boxsdk import Client
from boxsdk import JWTAuth
from boxsdk.exception import BoxAPIException

import json

class BoxAPI(object):

    def __init__(self, config_path="box_config.json"):
        config = JWTAuth.from_settings_file(config_path)

        self.config_path = config_path
        self.client = Client(config)

        # self.root_folder_id = "108724631596" # /TestProjects
        self.root_folder_id = "76450451998" # /Salesforce/Projects


    def search(self, parent_folder, name, is_folder=False):
        folder_items = parent_folder.get_items()

        file_type = "folder" if is_folder else "file"
        result = list(filter(lambda file: file.type == file_type and file.name == name, folder_items))
        return result[0] if len(result) else None



    def upload_files(self, project_name, binary_files):
        root_folder = self.client.folder(folder_id=self.root_folder_id)

        project_folder = self.search(root_folder, project_name, is_folder=True)
        if project_folder is None:
            project_folder = root_folder.create_subfolder(project_name)

        for binary_file in binary_files:
            if self.search(project_folder, binary_file["filename"]) is None:
                project_folder.upload_stream(binary_file["content"], binary_file["filename"])


    def get_user_info(self):
        return self.client.user().get()


    def print_folder_items(self, folder_id):
        folder = self.client.folder(folder_id=folder_id)
        folder_name = folder.get().name
        print("Name:", folder_name)

        folder_items = list(folder.get_items())
        print("Items:", folder_items)


def json_print(obj):
    print(json.dumps(obj, indent=4))


if __name__ == '__main__':
    ctr = BoxAPI()

    from io import BytesIO
    files = [
        {
            "filename": "F1.txt",
            "content": BytesIO(b"text1\n"),
        },
        {
            "filename": "F2.txt",
            "content": BytesIO(b"text2\n"),
        },
        {
            "filename": "F3.txt",
            "content": BytesIO(b"text3\n"),
        },
    ]
    # ctr.upload_files("Test Project1", files)

