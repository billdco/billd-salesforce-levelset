#!/usr/bin/env python3
from flask import abort, Flask, jsonify, Response, redirect, request, url_for

from coordinator import Coordinator

from threading import Thread
from functools import wraps


API_KEY = "billd-salesforce-levelset-api-key-1"

app = Flask(__name__)
app.api_coordinator = Coordinator()
application = app


def requires_api_key(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if request.args.get("api_key") == API_KEY:
            return f(*args, **kwargs)

        return abort(401)

    return decorated


@app.route("/", methods=["GET"])
@requires_api_key
def root():
    return Response("Salesforce-Levelset v-{} API by Outliant.".format("0.1.0"))


@app.route("/projects/<sf_project_id>", methods=["GET"])
@requires_api_key
def setup_levelset_project(sf_project_id):
    resp = app.api_coordinator.setup_new_levelset_project(sf_project_id)
    return jsonify(resp)


@app.route("/invoices/<sf_material_purchase_id>", methods=["GET"])
@requires_api_key
def create_or_update_invoice(sf_material_purchase_id):
    if request.args.get("create"):
        resp = app.api_coordinator.create_levelset_invoice(sf_material_purchase_id)
    else:
        resp = app.api_coordinator.update_levelset_invoice(sf_material_purchase_id)
    print(sf_material_purchase_id, resp)
    return jsonify(resp)


@app.route("/project_periodic_sync", methods=["GET"])
@requires_api_key
def get_projects_for_periodic_sync():
    project_ids = list(app.api_coordinator.get_projects_for_periodic_sync())
    return jsonify(project_ids)


@app.route("/project_periodic_sync/<levelset_project_id>/sync", methods=["GET"])
@requires_api_key
def sync_project(levelset_project_id):
    resp = app.api_coordinator.sync_project(levelset_project_id)
    return jsonify(resp)


@app.route("/sync_projects", methods=["GET"])
@requires_api_key
def sync_all_projects():
    worker = Thread(target=app.api_coordinator.sync_all_projects)
    worker.daemon = True
    worker.start()

    return jsonify({"message": "Projects syncing in background."})


@app.route("/project_periodic_sync/<sf_project_id>/remove", methods=["GET"])
@requires_api_key
def get_synced_projects(sf_project_id):
    resp = app.api_coordinator.remove_project_for_periodic_sync(sf_project_id)
    return jsonify(resp)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, threaded=False)
