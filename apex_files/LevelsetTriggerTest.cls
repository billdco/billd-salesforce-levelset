@isTest
private class LevelsetTriggerTest {
    class HttpMock implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest request) {
            // Create a fake response.
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{"test": "test"}');
            response.setStatusCode(200);
            return response;
        }
    }

    @IsTest
    static void testMaterialPurchaseTrigger() {
        Test.setMock(HttpCalloutMock.class, new HttpMock());

        Account testAccount = new Account(Name = 'Sample Account');
        insert testAccount;

        Project__c testProject = new Project__c();
        testProject.Name = 'Test Project';
        testProject.Contractor_Account__c = testAccount.Id;

        insert testProject;

        Opportunity testMaterialPurchase = new Opportunity();
        testMaterialPurchase.AccountId = testAccount.Id;
        testMaterialPurchase.Name = 'Test Material Purchase';
        testMaterialPurchase.StageName = 'Draft';
        testMaterialPurchase.Project__c = testProject.Id;
        testMaterialPurchase.CloseDate = system.today() + 1;

        insert testMaterialPurchase;

        testMaterialPurchase.Supplier_Disbursement_Date__c = Date.newInstance(2020, 4, 1);
        update testMaterialPurchase;
    }


    @IsTest
    static void testMaterialPurchaseTrigger_updateBalance() {
        Test.setMock(HttpCalloutMock.class, new HttpMock());

        Account testAccount = new Account(Name = 'Sample Account');
        insert testAccount;

        Project__c testProject = new Project__c();
        testProject.Name = 'Test Project';
        testProject.Contractor_Account__c = testAccount.Id;

        insert testProject;

        Opportunity testMaterialPurchase = new Opportunity();
        testMaterialPurchase.AccountId = testAccount.Id;
        testMaterialPurchase.Name = 'Test Material Purchase';
        testMaterialPurchase.StageName = 'Draft';
        testMaterialPurchase.Project__c = testProject.Id;
        testMaterialPurchase.CloseDate = system.today() + 1;

        insert testMaterialPurchase;

        // Create loan to update Material Purchase balance
        Loan__c loan = new Loan__c(
            Name ='Test Material Order Loan',
            Project__c = testProject.Id,
            Material_Order__c = testMaterialPurchase.Id,
            Contractor_Account__c = testAccount.Id,
            Loan_Balance__c = 10
        );
        insert loan;
    }


    @IsTest
    static void testProjectTrigger() {
        Test.setMock(HttpCalloutMock.class, new HttpMock());

        Account testAccount = new Account(Name = 'Sample Account');
        insert testAccount;

        Project__c testProject = new Project__c();
        testProject.Name = 'Test Project';
        testProject.Contractor_Account__c = testAccount.Id;

        insert testProject;

        testProject.Project_Status__c = 'Closed';
        update testProject;
    }
}
