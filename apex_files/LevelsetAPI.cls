public class LevelsetAPI {
    @future (callout=true)
    public static void sendInvoice(String opportunityId) {
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();

        req.setEndpoint('http://salesforce-levelset-prod.eba-nmvum9a7.us-west-2.elasticbeanstalk.com/invoices/'+opportunityId+'?api_key=billd-salesforce-levelset-api-key-1');
        req.setMethod('GET');
        try {
            res = http.send(req);
        } catch(System.CalloutException e) {
            System.debug('Callout error: '+ e);
            System.debug(res.toString());
        }
    }

    @future (callout=true)
    public static void turnOffProjectSync(String projectId) {
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();

        req.setEndpoint('http://salesforce-levelset-prod.eba-nmvum9a7.us-west-2.elasticbeanstalk.com/project_periodic_sync/'+projectId+'/remove?api_key=billd-salesforce-levelset-api-key-1');
        req.setMethod('GET');
        try {
            res = http.send(req);
        } catch(System.CalloutException e) {
            System.debug('Callout error: '+ e);
            System.debug(res.toString());
        }
    }
}
